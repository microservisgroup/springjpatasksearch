FROM alpine:3.14
RUN apk add --no-cache openjdk11
COPY build/libs/SpringJpaCourse-0.0.1-SNAPSHOT.jar /app/
CMD java -jar /app/SpringJpaCourse-0.0.1-SNAPSHOT.jar
