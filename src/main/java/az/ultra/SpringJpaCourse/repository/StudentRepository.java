package az.ultra.SpringJpaCourse.repository;

import az.ultra.SpringJpaCourse.dto.StudentDto;
import az.ultra.SpringJpaCourse.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository

public interface StudentRepository extends JpaRepository<Student,Long> {
    @Query(nativeQuery = false ,value = "select s from Student s where s.active=1")

    List<Student> findByActive();

    List<Student> findByFirstName(String firstName);

    List<Student>findByFirstNameAndLastNameAndMiddleName(String firstName, String lastName, String middleName);



}
