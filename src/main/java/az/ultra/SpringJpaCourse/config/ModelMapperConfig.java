package az.ultra.SpringJpaCourse.config;

import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;

@Configuration
@RequiredArgsConstructor
public class ModelMapperConfig {
private final EntityManagerFactory entityManagerFactory;
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper;
    }
@Bean
    public JPAStreamer jpaStreamer(){
        return JPAStreamer.of(entityManagerFactory);
}
}
