package az.ultra.SpringJpaCourse.dto;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
//@RequiredArgsConstructor

public class StudentDto {
  //  @NonNull
    private String firstName;
  //  @NonNull
    private String lastName;
  //  @NonNull
    private String middleName;
  //  @NonNull
    private String groupNumber;
    public StudentDto(){

    }
}
