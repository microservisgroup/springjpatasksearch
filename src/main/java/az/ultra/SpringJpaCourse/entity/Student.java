package az.ultra.SpringJpaCourse.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Student {
    @Id
    private long id;

    private String firstName;
    private String lastName;
    private String middleName;
    private String groupNumber;
    private int active;

}
