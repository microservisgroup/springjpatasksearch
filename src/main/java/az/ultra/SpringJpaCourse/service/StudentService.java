package az.ultra.SpringJpaCourse.service;

import az.ultra.SpringJpaCourse.dto.StudentDto;
import az.ultra.SpringJpaCourse.entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface StudentService {
public List<Student> getAllStudents();
public List<StudentDto>getActiveStudents();
public StudentDto saveStudent(StudentDto student);

    List<StudentDto> findByFirstName(String firstName);
    List<StudentDto>findByFirstNameAndLastNameAndMiddleName(String firstName,String lastName,String middleName);

    List<StudentDto> findByJpaStreamer(String firstName, String lastName, String middleName);

    List<StudentDto> findByExampleMatcher(Student student);
}
