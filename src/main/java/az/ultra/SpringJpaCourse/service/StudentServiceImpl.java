package az.ultra.SpringJpaCourse.service;

import az.ultra.SpringJpaCourse.dto.StudentDto;
import az.ultra.SpringJpaCourse.entity.Student;
import az.ultra.SpringJpaCourse.entity.Student$;
import az.ultra.SpringJpaCourse.repository.StudentRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;
    private final JPAStreamer jpaStreamer;
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }
     public List<StudentDto> getActiveStudents(){
         List<StudentDto> studentDtoList = studentRepository.findByActive()
                 .stream()
                 .map(student -> modelMapper.map(student, StudentDto.class))
                 .collect(Collectors.toList());
        return studentDtoList;
 }
    @Override
    public StudentDto saveStudent(StudentDto studentDto) {
       Student student=modelMapper.map(studentDto,Student.class);
       return modelMapper.map(studentRepository.save(student),StudentDto.class);
    }

    @Override
    public List<StudentDto> findByFirstName(String firstName) {
        List<StudentDto> studentDtoList = studentRepository.findByFirstName(firstName)
                .stream()
                .map(student -> modelMapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
        return  studentDtoList;
    }

    @Override
    public List<StudentDto> findByFirstNameAndLastNameAndMiddleName(String firstName, String lastName, String middleName) {
        List<StudentDto> studentDtoList = studentRepository.findByFirstNameAndLastNameAndMiddleName(firstName,lastName,middleName)
                .stream()
                .map(student -> modelMapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
        return  studentDtoList;
    }

    @Override
    public List<StudentDto> findByJpaStreamer(String firstName, String lastName, String middleName) {
       List<Student> studentList=  jpaStreamer.stream(Student.class)
                .filter(Student$.firstName.contains(firstName))
                 .filter(Student$.lastName.contains(lastName))
               .filter(Student$.middleName.contains(middleName))
               .collect(Collectors.toList());
        List<StudentDto> studentDtoList = modelMapper.map(studentList,new TypeToken<List<StudentDto>>(){}.getType());
        return studentDtoList;


    }

    @Override
    public List<StudentDto> findByExampleMatcher(Student student) {
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAll()
                .withIgnorePaths("id")
              //  .withIgnoreCase("firstName")
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreNullValues();
        Example<Student> personExample = Example.of(student, exampleMatcher);
        List<Student> studentList=studentRepository.findAll(personExample);
        List<StudentDto> studentDtoList = modelMapper.map(studentList,new TypeToken<List<StudentDto>>(){}.getType());
        return studentDtoList;

    }

}
