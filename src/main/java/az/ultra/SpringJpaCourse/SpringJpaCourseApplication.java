package az.ultra.SpringJpaCourse;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaCourseApplication  {

	public static void main(String[] args) {
		System.out.println("spring started");
		SpringApplication.run(SpringJpaCourseApplication.class, args);
	}


}
