package az.ultra.SpringJpaCourse.controller;

import az.ultra.SpringJpaCourse.dto.StudentDto;
//import az.ultra.SpringJpaCourse.service.StudentService;
import az.ultra.SpringJpaCourse.entity.Student;
import az.ultra.SpringJpaCourse.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
 private final StudentService studentService;
 public StudentController(StudentService studentService){
     this.studentService=studentService;
 }

 @GetMapping("/list")
    public List<Student> getStudents(){
    List<Student> studentDtoLit=studentService.getAllStudents();
    return studentDtoLit;
    }
//    @GetMapping("/activeList")
//    public List<StudentDto> getActiveStudents(){
//        List<StudentDto> studentDtoLit=studentService.getActiveStudents();
//        return studentDtoLit;
//    }
//    @GetMapping("/searchStudent")
//    public List<StudentDto> searchStudentByFirstName(@RequestParam String firstName){
//        System.out.println("firtsname="+firstName);
//     List<StudentDto> studentDtoList=studentService.findByFirstName(firstName);
//     return studentDtoList;
//    }
//    @GetMapping("/searchStudentByAllParam")
//    public List<StudentDto> searchStudentByAllNames(@RequestParam String firstName,String lastName,String middleName){
//        List<StudentDto> studentDtoList=studentService.findByFirstNameAndLastNameAndMiddleName(firstName,lastName,middleName);
//        return studentDtoList;
//    }

//    @GetMapping("/searchByJpaStreamer")
//    public List<StudentDto> searchStudentByJpaStreamer(@RequestParam String firstName,String lastName,String middleName){
//        List<StudentDto> studentDtoList=studentService.findByJpaStreamer(firstName,lastName,middleName);
//        return studentDtoList;
//    }
    @PostMapping("/search")
    public List<StudentDto> searchStudentByJpaStreamer(@RequestBody Student student){
        List<StudentDto> studentDtoList=studentService.findByExampleMatcher(student);
        return studentDtoList;
    }
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public StudentDto saveStudent(@Validated @RequestBody StudentDto studentDto){
//     return studentService.saveStudent(studentDto);
//
//    }
}
